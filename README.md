# BC Shelf Oceanographic Data Layers

__Main author:__  Jessica Nephin     
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@gmail.com | tel: 250-363-6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To produce raster surfaces for environmental variables from the BC ROMS regional circulation model to be used as predictors in species distribution models.


## Summary
Calculates summer and winter averages and ranges of bottom level temperature, salinity, and currents (circulation and tidal) over 10 years from 1998 to 2007. Variables were sourced from the BC Regional Ocean Modeling System (ROMS) circulation model (Masson and Fine, 2012). The model domain extends from the Columbia River to the Alaska Panhandle and has a 3 by 3 km grid resolution. The model includes forcing from 8 tidal constituents, river discharge, wind and other atmospheric conditions and boundary conditions. Model outputs were provided by Isaac Fine (DFO).  


## Status
Completed


## Contents
This projects contains R scripts that extract the values at the surface, bottom and water column bins, calculate seasonal means and ranges and exports the data as shapefiles. The SplineBarriers.py Python script creates raster layers by interpolating the point data.


## Methods
The ROMS model output was provided for two different temporal resolutions. The 'bcc30' data, with 15 day time steps, was used to calculate salinity, temperature and circulation current speed and the 'short3h' data, with 3 hour time steps, was used to calculate tidal current speed. Current velocities can be used to represent different components of the currents depending on the temporal resolution of averaging in the ROMS model outputs. The velocities from the 15 day model output can be used to represent general circulation patterns and the velocities from the 3 hour model output can resolve the tidal currents. To calculate current speed from the u and v directional velocities, they need to be horizontally shifted to the central nodes, which are the rho points where salinity and temperature values are located. Velocities were shifted horizontally using linear interpolation. Then speed was calculated from u and v and averaged temporally via root mean square (rms) method.

All variables (current speed, temperature and salinity) were averaged seasonally for each year. Seasons were winter (October, November, December, January, February, March) and summer (April, May, June, July, August, September). The seasonal data from each year were then averaged over the 10 years of data from 1998 to 2007. Variables were vertically aggregated by the following depth bins: surface, 10m, 25m, 50m, 75m, 100m, 150m, 200m, 250m, 250m to bottom and bottom. Depth layer files that related sigma levels to depth were used to assign each sigma level to a depth bin.

Point data can be interpolated to raster layers for each field as needed using the 'SplineBarriers.py' script.

**File naming convention**     
Shapefile names follow the naming convention: 'model-type_variable_depth-bin'. For example, 'bcc30_curr_10' = circulation current speed from surface to 10m depth.      

**Depth levels**     
s     = Surface layer     
10m   = surface to 10 m depth    
25m   = 10 to 25 m depth    
50m   = 25 to 50 m depth    
75m   = 50 to 75 m depth    
100m  = 75 to 100 m depth    
150m  = 100 to 150 m depth    
200m  = 150 to 200 m depth    
250m  = 200 to 250 m depth    
250tb = 250m to bottom     
b     = bottom layer       


## Requirements
*  Data processing was completed in R using ncdf4, abind, sp, rgdal and parallel
packages and Python using the arcpy module which requires an ESRI ArcMap and Spatial Analyst license.
*  A polygon of the study area for clipping raster layers to reduce extrapolation past the model points.


## Caveats
These layers are intended to represent long-term average variables at a 3 km resolution. They are not intended to represent fine scale spatial or temporal changes.


## Uncertainty
Interpolated (raster) data may extend past the model domain in several inlets, rivers and estuaries. The interpolated data should be constrained to the modelling extent or used/interpreted with caution in those areas. For uncertainties associated with the modelled variables see the source data, see Masson and Fine (2012) for more information.


## Acknowledgements
Isaac Fine, Diane Masson


## References
Masson, D., and I. Fine (2012), Modeling seasonal to interannual ocean
variability of coastal British Columbia, J. Geophys. Res., 117, C10019,
doi:10.1029/2012JC008151
