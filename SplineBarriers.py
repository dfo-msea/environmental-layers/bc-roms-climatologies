# Name: SplineBarriers.py
# Description: Interpolate a series of point features onto a
#    rectangular raster using a barrier, using a
#    minimum curvature spline technique.
# Requirements: Spatial Analyst Extension and Java Runtime
# Author: Jessica Nephin

# Import system modules
import os
import arcpy
from arcpy import env
from arcpy.sa import *

# set workspace
#os.chdir("..")

# Checkout
arcpy.CheckOutExtension('Spatial')

# type
model = "short3h" #bcc30, short3h
variables = ["curr",] #"temp","salt"
depth = ["b",] #"s"]
fields = ["range",] #"min","max","meanSummer","meanWinter"]

#------------------------------------------------------------------#
# for variable
for v in variables:

    #------------------------------------------------------------------#
    # for depth bin
    for d in depth:

        # variable and path
        path = "/"+model+"/Data/"+v+"/"
        var = model+"_"+v+"_"+d

        #------------------------------------------------------------------#
        for zField in fields:

            # Set local variables
            infeature = os.getcwd()+path+"Shapefiles/"+var+".shp"
            inBarrier =  os.getcwd()+"/Boundary/coast_buffer_2km.shp"
            cellSize =  1000.0
            smooth = 1

            # Execute Spline with Barriers
            outSB = SplineWithBarriers(infeature, zField, inBarrier, cellSize, smooth)

            # Clip with EEZ polygon
            inMask =  os.getcwd()+"/Boundary/EEZ_alb_1km_ROMS.shp"
            outExtractByMask = ExtractByMask(outSB, inMask)

            # Re-classify values less than zero to zero
            outReclass = Con(outExtractByMask, 0, outExtractByMask, "VALUE < 0")

            # output name
            outfilename = os.getcwd()+path+"Rasters/"+var+"_"+zField+".tif"

            # Save the output
            outReclass.save(outfilename)
